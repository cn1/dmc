#! /usr/bin/python3

# SPDX-License-Identifier: MIT-0 OR Apache-2.0

from lark import Lark

from lark import Lark, Transformer, v_args

@v_args(inline=True)    # Affects the signatures of the methods
class DMC_Tree(Transformer):
    from operator import add, sub, mul, truediv as div, neg
    number = float

    def __init__(self):
        self.vars = {}

    def assign_var(self, name, value):
        self.vars[name] = value
        #return value

    def var(self, name):
        try:
            return self.vars[name]
        except KeyError:
            raise Exception("Variable not found: %s" % name)

class DMC:
    def __init__(self):
        dmc_grammar = r"""
            ?start: stmts
            ?stmts: ( stmt [";"] )*
            ?stmt: sum
                  | NAME "=" sum    -> assign_var
            ?sum: product
                | sum "+" product   -> add
                | sum "-" product   -> sub
            ?product: atom
                | product "*" atom  -> mul
                | product "/" atom  -> div
            ?atom: NUMBER           -> number
                 | "-" atom         -> neg
                 | NAME             -> var
                 | "(" sum ")"
            %import common.CNAME -> NAME
            %import common.NUMBER
            %import common.WS_INLINE
            %ignore WS_INLINE
"""
        self.parser = Lark(dmc_grammar, parser='lalr',
                            transformer=DMC_Tree())
        self.interp = self.parser.parse

    def run(self):
        while True:
            try:
                s = input('> ')
            except EOFError:
                break
            print(self.interp(s))

if __name__ == '__main__':
    dmc = DMC()
    dmc.run()
