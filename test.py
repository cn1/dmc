#! /usr/bin/python3

import unittest
import dmc

class Test(unittest.TestCase):
    def setUp(self):
        self.dmc = dmc.DMC()
    def test_simple(self):
        self.assertEqual(6, self.dmc.interp("3+3"))
    def test_StmtSemi(self):
        self.dmc.interp("a=1;")
    def test_2Assignments(self):
        self.dmc.interp("a=1; b=2")
    def test_Var(self):
        self.dmc.interp("a = 1+2")
        self.assertEqual(3, self.dmc.interp("a"))
        self.assertEqual(-8, self.dmc.interp("1+a*-3"))

if '__main__' == __name__:
    unittest.main()
